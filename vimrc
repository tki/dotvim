call plug#begin('~/.vim/plugged')

Plug 'tomtom/tlib_vim'
Plug 'junegunn/vim-easy-align'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'scrooloose/nerdcommenter'
Plug 'chriskempson/base16-vim'
Plug 'kchmck/vim-coffee-script'
Plug 'mtscout6/vim-cjsx'
Plug 'kien/ctrlp.vim'
Plug 'Lokaltog/vim-easymotion'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'justinj/vim-react-snippets'
Plug 'szw/vim-ctrlspace'
Plug 'tpope/vim-sensible'
Plug 'tpope/gem-ctags'
Plug 'mxw/vim-jsx'
Plug 'Slava/vim-spacebars'
Plug 'tpope/vim-surround'
Plug 'mattn/emmet-vim'

"themes
Plug 'whatyouhide/vim-gotham'
Plug 'mtglsk/mushroom'
Plug 'ajh17/Spacegray.vim'
Plug 'morhetz/gruvbox'

call plug#end()

filetype plugin indent on
set autoindent
let base16colorspace=256

syntax on

set nobackup
set background=dark
set guioptions=*
set guifont=Source\ Code\ Pro\ for\ Powerline:h14
set expandtab
set tabstop=2
set shiftwidth=2

set t_Co=256

colorscheme gotham256
"colorscheme mushroom

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

set wildignore+=*/tmp/*,*.so,*.swp,*.zip
let g:ctrlp_clear_cache_on_exit = 1

let mapleader=","

autocmd BufWritePre * :%s/\s\+$//e
au BufReadPost *.html set syntax=spacebars

map <Leader>n :NERDTreeToggle<CR>
nmap <Leader>m :NERDTreeFind<CR>
nnoremap <F3> :NumbersToggle<CR>
vmap <Enter> <Plug>(EasyAlign)
